<?php

abstract class Conta //atribuir set pegar o valor get                                        
{
    public $tipoDaConta;
    public function getTipoDaConta(){
        return $this-> tipoDaConta;
    }
    public function setTipoDaCoonta(String $tipoDaConta){
        $this-> tipoDaConta = $tipoDaConta;

    }


    public $agencia;
    public $conta;
    protected $saldo; //(protected só permite acesso dentro da classe ou na classe filha) se for private, é possivel acessar dentro da classe pai. 

    public function ImprimeExtrato() //metodo concreto
    {
        echo "Conta: ". $this->tipoDaConta. " Agência: ". $this->agencia. " Conta: ". $this->conta. " Saldo: ". $this->saldo . ' Extrato : ' . $this->calculaSaldo();
    }

     public function deposito(float $valor)
     {
        // $this->saldo = $this->saldo + $valor; //quando quer acessar o atributo
         //$this-> saldo -= $valor

         if($valor>0)
         {
            $this->saldo +=$valor;
             echo "Deposito realizado com sucesso <br>";
         } 
         else 
         {
             echo "Deposito negativo!! <br>";
         }

         
     }


     public function saque(float $valor)
     {
         //$this->saldo = $this->saldo - $valor;

         if($this->saldo >= $valor)
         {
             $this->saldo -=$valor;
             echo "Saque realizado com Sucesso <br>";
         } 
         else 
         {
             echo "Saque insuficiente!!! <br>";
         }

     }

     abstract public function calculaSaldo(); // metodo abstrado
}



    class Poupanca extends Conta
    {
        public $reajuste;

        public function __construct(string $agencia, string $conta, float $reajuste) //o construtor inicializa um objeto
        {
            $this->setTipoDaCoonta( 'Poupança');
            $this->agencia= $agencia;
            $this->conta= $conta;
            $this->reajuste = $reajuste;
        }

    public function calculaSaldo()
    {
        return $this->saldo + ($this->saldo * $this->reajuste/100);
    }
    }

class Especial extends Conta
{
    public $saldoEspecial;

    public function __construct(string $agencia, string $conta, float $saldoEspecial)
    {
        $this->tipoDaConta= 'Especial';
        $this->agencia= $agencia;
        $this->conta= $conta;
        $this->saldoEspecial = $saldoEspecial;
    }

    public function calculaSaldo()//metodo
    {
        return $this->saldo + $this->saldoEspecial;
    }
}


$ctaPoup= new Poupanca('0002-7', '85588-88', 0.54);
//$ctaPoup-> saldo = -1500 (não pode acessar atributo protegido)
$ctaPoup-> deposito(-1500);
$ctaPoup-> saque(300);
$ctaPoup-> ImprimeExtrato();

echo'<br>';

$ctaEspecial = new Especial('0055-2', '88', 0.54);
$ctaEspecial-> deposito(1500);
$ctaEspecial-> ImprimeExtrato();
