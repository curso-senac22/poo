<?php


abstract class Forma
{
    public $tipoDeForma = 'forma Abstrata';
    public function ImprimeForma()
    {
        echo $this->tipoDeForma . ' com Área de: ' . $this->calculaArea();
    }
    abstract public function calculaArea();
}

class Quadrado extends Forma
{
    public $lado;

    public function __construct(float $varLado)
    {
        $this-> tipoDeForma = 'Quadrado';
        $this-> lado = $varLado;
    }

    public function calculaArea()
    {
        return $this->lado * $this->lado;
    }
}

class Retangulo extends Forma
{
    public $base;
    public $altura;

    public function __construct(float $varBase, $varAltura)
    {
        $this-> tipoDeForma = 'Retangulo';
        $this-> base = $varBase;
        $this->altura= $varAltura;
    }
    public function calculaArea()
    {
        return $this->base * $this->altura;
    }
}


class Triangulo extends Forma
{
    public $cumprimentoBase;
    public $alturaTri;

    public function __construct(float $cumprimentoBase, $alturaTri)
    {
        $this-> tipoDeForma = 'Triangulo';
        $this-> cumprimentoBase = $cumprimentoBase;
        $this-> alturaTri = $alturaTri;
    }
    public function calculaArea()
    {
        return $this->cumprimentoBase * $this->alturaTri / 2;
    }
}



$obj= new Quadrado(5);
$obj->imprimeForma();

$obj1= new Quadrado(100);
$obj1-> ImprimeForma();

$obj2 = new Retangulo(5, 10);
$obj2 ->ImprimeForma();

$obj3 = new Triangulo(6, 10);
$obj3-> ImprimeForma();

//$obj = new Forma();

//$obj -> imprimeForma();
